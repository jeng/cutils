--
-- cutils
--
  solution 'cutils'
    configurations { 'Release', 'Debug' }

  project 'cutils'
    targetname      'cutils'
    targetdir       'lib'
    language        'C'
    kind            'StaticLib'
    flags           { 'ExtraWarnings', 'FatalWarnings' }
    includedirs     { 'include' }
    libdirs         { 'lib' }
    buildoptions    {}
    linkoptions     {}

    files {
      'src/*.c',
    }

    if os.is('windows') then
      excludes {
        'src/util.c',
        'src/*socket*.c'
      }
    end

    configuration 'Debug'
      targetsuffix    '-d'
      flags           'Symbols'
      defines         '_DEBUG'

    configuration 'Release'
      flags           'OptimizeSize'
      defines         'NDEBUG'

  project 'cutils_test'
    targetname      'cutils_test'
    targetdir       'bin'
    language        'C'
    kind            'ConsoleApp'
    flags           {}
    includedirs     { 'include', '/usr/local/include', 'test' }
    libdirs         { 'lib', '/usr/local/lib' }
    if os.is('windows') then
      links { 'cutils' }
    elseif os.is('bsd') then
      links { 'cutils', 'm', 'pthread' }
    else
      links { 'cutils', 'm', 'dl', 'pthread' }
    end
    buildoptions    {}
    linkoptions     {}

    files {
      'test/**.c',
    }

    excludes {
    }

    configuration 'Debug'
      targetsuffix    '-d'
      flags           'Symbols'
      defines         '_DEBUG'

    configuration 'Release'
      flags           'OptimizeSize'
      defines         'NDEBUG'

-- vim: set ft=lua ts=8 sw=2 sts=2 expandtab:
