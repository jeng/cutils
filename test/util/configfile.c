/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#include "clay_cutils.h"
#include "cutils/configfile.h"

#define V(x) configfile_str(cfg, x, NULL)

void test_util_configfile__from_str(void)
{
    char buf[] =  "pep {\n" \
                  "  ; count\n" \
                  "  cnt 2\n" \
                  "  yugi {\n" \
                  "    # jim\n" \
                  "    name = \"jim green\"\n" \
                  "    age : 12\n" \
                  "  }\n" \
                  "}\n";
    configfile_t *cfg = configfile_create(buf);
    cl_assert(cfg != NULL);
    cl_assert(streq(V("pep.cnt"), "2"));
    cl_assert(streq(V("pep.yugi.name"), "jim green"));
    cl_assert(streq(V("pep.yugi.age"), "12"));
    cl_assert(V("pep") == NULL);
    configfile_destroy(cfg);
}

void _test_util_configfile__from_file(void)
{
    configfile_t *cfg = configfile_create_from_file("cfg");
    cl_assert(cfg != NULL);
    cl_assert(streq(V("pep.cnt"), "2"));
    cl_assert(streq(V("pep.yugi.name"), "jim green"));
    cl_assert(streq(V("pep.yugi.age"), "12"));
    cl_assert(V("pep") == NULL);
    configfile_destroy(cfg);
}
