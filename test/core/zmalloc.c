/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#include "clay_cutils.h"

void test_core_zmalloc__zmalloc(void)
{
    void *p = zmalloc(1024);
    cl_assert(p != NULL);
    zfree(p);
    cl_assert(zmalloc_used_memory() == 0);
}

void test_core_zmalloc__zcalloc(void)
{
    void *p = zcalloc(5, 1024);
    cl_assert(p != NULL);
    zfree(p);
    cl_assert(zmalloc_used_memory() == 0);
}

void test_core_zmalloc__zstrdup(void)
{
    char *p = zstrdup("hijcak");
    cl_assert(streq(p, "hijcak"));
    zfree(p);
    cl_assert(zmalloc_used_memory() == 0);
}

void test_core_zmalloc__zstrndup(void)
{
    char *p = zstrndup("hijcak", 2);
    cl_assert(streq(p, "hi"));
    zfree(p);

    p = zstrndup("hijcak", 6);
    cl_assert(streq(p, "hijcak"));
    zfree(p);

    p = zstrndup("hijcak", 10);
    cl_assert(streq(p, "hijcak"));
    zfree(p);

    cl_assert(zmalloc_used_memory() == 0);
}
