/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CLAY_CUTILS_H_INCLUDED__
#define __CLAY_CUTILS_H_INCLUDED__

#include "clay.h"
#include "cutils.h"

#ifdef __cplusplus
extern "C" {
#endif

#define cl_cutils_pass(expr) do { \
    error_clear(); \
    if ((expr) != CUTILS_OK) \
        clay__assert(0, __FILE__, __LINE__, "Function call failed: " #expr, error_last(), 1); \
    } while(0);

#define cl_cutils_fail(expr) cl_must_fail((expr))

#define streq(a, b) (!strcmp((a), (b)))

#ifdef __cplusplus
}
#endif

#endif /* __CLAY_CUTILS_H_INCLUDED__ */
