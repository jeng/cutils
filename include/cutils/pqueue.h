/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_PQUEUE_H_INCLUDED__
#define __CUTILS_PQUEUE_H_INCLUDED__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef int (*pqueue_cmp_pfn)(void *a, void *b);

/** the priority queue handle */
typedef struct {
    size_t size, avail, step;
    pqueue_cmp_pfn cmp;
    void **d;
} pqueue_t;

/** initialize the queue */
int pqueue_init(pqueue_t *q, size_t n, pqueue_cmp_pfn cmp);

/** free all memory used by the queue */
void pqueue_close(pqueue_t *q);

/** clear all the elements in the queue */
void pqueue_clear(pqueue_t *q);

/** return the size of the queue */
size_t pqueue_size(pqueue_t *q);

/** insert an item into the queue */
int pqueue_insert(pqueue_t *q, void *d);

/** pop the highest-ranking item from the queue */
void *pqueue_pop(pqueue_t *q);

/** access highest-ranking item without removing it */
void *pqueue_peek(pqueue_t *q);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_PQUEUE_H_INCLUDED__ */
