/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_PRELUDE_H_INCLUDED__
#define __CUTILS_PRELUDE_H_INCLUDED__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __GNUC__
    #define CUTILS_FORMAT_PRINTF(a,b) __attribute__((format (printf, a, b)))
#else
    #define CUTILS_FORMAT_PRINTF(a,b) /* empty */
#endif

#if defined(_WIN32) && !defined(__CYGWIN__)
    #define CUTILS_WIN32 1
#endif

#define container_of(ptr_, type_, member_)  \
    ((type_ *)((char *)ptr_ - offsetof(type_, member_)))

#ifndef NULL
    #ifdef __cplusplus
        #define NULL        (0L)
    #else
        #define NULL        ((void*)0)
    #endif
#endif

#if (!defined(min))
    #define min(a, b)       (((a) < (b))? (a): (b))
    #define max(a, b)       (((a) > (b))? (a): (b))
#endif

#if (!defined(TRUE))
    #define TRUE    1
    #define FALSE   0
#endif

#if (!defined(EXIT_SUCCESS))
    #define EXIT_SUCCESS    0
    #define EXIT_FAILURE    1
#endif

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
    #define CUTILS_ASSERT_SANE_FUNCTION    __func__
#elif defined(__GNUC__) && (__GNUC__ >= 2)
    #define CUTILS_ASSERT_SANE_FUNCTION    __FUNCTION__
#else
    #define CUTILS_ASSERT_SANE_FUNCTION    "<unknown>"
#endif

#ifdef CUTILS_WIN32
    #define CUTILS_PATH_LIST_SEPARATOR ';'
#else
    #define CUTILS_PATH_LIST_SEPARATOR ':'
#endif

#define CUTILS_PATH_MAX 4096

#ifdef __cplusplus
    #define CUTILS_UNUSED(x)
#else
    #ifdef __GNUC__
        #define CUTILS_UNUSED(x) x __attribute__ ((__unused__))
    #else
        #define CUTILS_UNUSED(x) x
    #endif
#endif

#if defined(_MSC_VER)
    #define CUTILS_UNUSED_ARG(x) ((void)(x)); /* note trailing ; */
#else
    #define CUTILS_UNUSED_ARG(x)
#endif

#define CUTILS_ARRAY_SIZE(x)       (sizeof(x)/sizeof(x[0]))

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_PRELUDE_H_INCLUDED__ */
