/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_MD5_H_INCLUDED__
#define __CUTILS_MD5_H_INCLUDED__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t u32;
typedef uint8_t u8;

struct MD5Context {
    u32 buf[4];
    u32 bits[2];
    u8 in[64];
};
typedef struct MD5Context MD5_CTX;

void MD5Init(struct MD5Context *ctx);

void MD5Update(struct MD5Context *ctx, unsigned char const *buf, unsigned len);

void MD5Final(unsigned char digest[16], struct MD5Context *ctx);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_MD5_H_INCLUDED__ */
