/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_ASOCKET_H_INCLUDED__
#define __CUTILS_ASOCKET_H_INCLUDED__

#include <stdlib.h>
#include <sys/socket.h>

#ifdef __cplusplus
extern "C" {
#endif

struct asocket {
    int fd;
    int abort_fd[2];
};

/** create an asocket from fd */
struct asocket *asocket_create(int fd);

int asocket_connect(struct asocket *s, const struct sockaddr *addr,
        socklen_t addrlen, int timeout);

int asocket_accept(struct asocket *s, struct sockaddr *addr,
        socklen_t *addrlen, int timeout);

int asocket_read(struct asocket *s, void *buf, size_t count, int timeout);

int asocket_write(struct asocket *s, const void *buf, size_t count,
        int timeout);

/** abort above calls and shutdown socket */
void asocket_abort(struct asocket *s);

/** close socket and free asocket structure */
void asocket_destroy(struct asocket *s);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_ERROR_H_INCLUDED__ */
