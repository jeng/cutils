/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_SOCKET_H_INCLUDED__
#define __CUTILS_SOCKET_H_INCLUDED__

#ifdef __cplusplus
extern "C" {
#endif

int socket_tcp_connect(char *err, char *addr, int port, int nonblock);
int socket_unix_connect(char *err, char *path, int nonblock);

int socket_read(int fd, char *buf, int count);
int socket_write(int fd, char *buf, int count);

int socket_tcp_server(char *err, int port, char *bindaddr);
int socket_unix_server(char *err, char *path, mode_t perm);

int socket_tcp_accept(char *err, int s, char *ip, int *port);
int socket_unix_accept(char *err, int s);

int socket_resolve(char *err, char *host, char *ipbuf);

int socket_set_nonblock(char *err, int fd);
int socket_set_tcpnodelay(char *err, int fd);
int socket_set_tcpkeepalive(char *err, int fd);

int socket_peertostring(int fd, char *ip, int *port);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_SOCKET_H_INCLUDED__ */
