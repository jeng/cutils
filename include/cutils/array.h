/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_ARRAY_H_INCLUDED__
#define __CUTILS_ARRAY_H_INCLUDED__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    size_t selem; /** size of a single element */
    int nelem; /** number of elements */
    int nalloc; /** number of allocated elements */
    char *elems; /** ptr of elements */
} array_t;

/** init the dynamic array struct */
int array_init(array_t *arr, int nelem, size_t selem);

/** free element's memory space and set array size to 0 */
void array_close(array_t *arr);

/** push an element to array tail*/
void *array_push(array_t *arr);

/** pop array's tail element */
void *array_pop(array_t *arr);

/** clear array and set size to 0 but memory space not free */
void array_clear(array_t *arr);

/** check array empty or not */
#define array_isempty(arr) (((arr)->nelem == 0) ? 1 : 0)

/** get the array size */
#define array_size(arr) ((arr)->nelem)

/** fetch element by index */
#define ARRAY_IDX(arr, i, type) (((type *)((arr)->elems))[i])

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_ARRAY_H_INCLUDED__ */
