/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_UTF8_H_INCLUDED__
#define __CUTILS_UTF8_H_INCLUDED__

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned short char32_t;

size_t utf8tochar32(const char *utf8, char32_t *utf16, size_t sz);
size_t utf8fromchar32(const char32_t *utf16, char *utf8, size_t sz);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_UTF8_H_INCLUDED__ */
