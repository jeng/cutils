/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_ERROR_H_INCLUDED__
#define __CUTILS_ERROR_H_INCLUDED__

#ifdef __cplusplus
extern "C" {
#endif

#define CUTILS_OK 0
#define CUTILS_ERR -1
#define CUTILS_ENOMEM -1001
#define CUTILS_ENOTFOUND -1002
#define CUTILS_EOSERR -1003

/** get the last error str */
const char *error_last(void);

/** convert a error code to str */
const char *error_tostr(int err);

/** clear error */
void error_clear(void);

/** throw a error with msg */
int error_throw(int err, const char *msg, ...);

/** rethrow a error with msg */
int error_rethrow(int err, const char *msg, ...);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_ERROR_H_INCLUDED__ */
