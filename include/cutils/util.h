/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_UTIL_H_INCLUDED__
#define __CUTILS_UTIL_H_INCLUDED__

#ifdef __cplusplus
extern "C" {
#endif

void p_srand(int seed);
int p_rand(void);

char *p_strcpyn(char *dst, const char *src, size_t dst_size);
char *p_strcatn(char *dst, const char *src, size_t dst_size);
int p_strmatch(const char *pattern, const char *string, int nocase);
long long p_memtoll(const char *p, int *err);
int p_ll2str(char *s, size_t len, long long value);
int p_str2ll(char *s, size_t slen, long long *value);
int p_str2l(char *s, size_t slen, long *lval);
int p_prefixcmp(const char *str, const char *prefix);
int p_suffixcmp(const char *str, const char *suffix);
char *p_strtok(char **end, const char *sep);
void p_hexdump(const char *buffer, size_t len);

void *p_load_file(const char *fn, unsigned *sz_);
int p_is_little_endian(void);

long long p_ustime(void);
void p_mssleep(int ms);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_UTIL_H_INCLUDED__ */
