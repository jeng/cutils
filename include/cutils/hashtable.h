/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_HASHTABLE_H_INCLUDED__
#define __CUTILS_HASHTABLE_H_INCLUDED__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef size_t (*key_hash_fn)(const void *key);
typedef int (*key_cmp_fn)(const void *key1, const void *key2);
typedef void (*free_fn)(void *key);

extern size_t hashtable_str_hash(const void *key);
extern int hashtable_str_cmp(const void *key1, const void *key2);

struct hashtable_list {
    struct hashtable_list *prev;
    struct hashtable_list *next;
};

struct hashtable_pair {
    void *key;
    void *value;
    size_t hash;
    struct hashtable_list list;
};

struct hashtable_bucket {
    struct hashtable_list *first;
    struct hashtable_list *last;
};

typedef struct hashtable {
    size_t size;
    struct hashtable_bucket *buckets;
    size_t num_buckets; /* index to primes[] */
    struct hashtable_list list;

    key_hash_fn hash_key;
    key_cmp_fn cmp_keys; /* returns non-zero for equal keys */
    free_fn free_key;
    free_fn free_value;
} hashtable_t;

/** create a hashtable object */
hashtable_t *hashtable_create(key_hash_fn hash_key, key_cmp_fn cmp_keys,
                              free_fn free_key, free_fn free_value);

/** destroy a hashtable object */
void hashtable_destroy(hashtable_t *hashtable);

/** init a hashtable object */
int hashtable_init(hashtable_t *hashtable,
                   key_hash_fn hash_key, key_cmp_fn cmp_keys,
                   free_fn free_key, free_fn free_value);

/** close a hashtable object */
void hashtable_close(hashtable_t *hashtable);

/** add or modify value in hashtable, internal free key and value if not NULL */
int hashtable_set(hashtable_t *hashtable, void *key, void *value);

/** get a value associated with a key */
void *hashtable_get(hashtable_t *hashtable, const void *key);

/** remove a value from the hashtable */
int hashtable_del(hashtable_t *hashtable, const void *key);

/** clear hashtable */
void hashtable_clear(hashtable_t *hashtable);

/** iterate over hashtable and return an iterator object */
void *hashtable_iter(hashtable_t *hashtable);

/** return an iterator at a specific key */
void *hashtable_iter_at(hashtable_t *hashtable, const void *key);

/** advance an iterator */
void *hashtable_iter_next(hashtable_t *hashtable, void *iter);

/** retrieve the key pointed by an iterator */
void *hashtable_iter_key(void *iter);

/** rtrieve the value pointed by an iterator */
void *hashtable_iter_value(void *iter);

/** set the value pointed by an iterator */
void hashtable_iter_set(hashtable_t *hashtable, void *iter, void *value);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_HASHTABLE_H_INCLUDED__ */
