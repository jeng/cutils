/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_SHA1_H_INCLUDED__
#define __CUTILS_SHA1_H_INCLUDED__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t u_int32_t;

typedef struct {
    u_int32_t state[5];
    u_int32_t count[2];
    unsigned char buffer[64];
} SHA1_CTX;

void SHA1Init(SHA1_CTX* context);

void SHA1Update(SHA1_CTX* context, const unsigned char* data, u_int32_t len);

void SHA1Final(unsigned char digest[20], SHA1_CTX* context);

void SHA1Transform(u_int32_t state[5], const unsigned char buffer[64]);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_SHA1_H_INCLUDED__ */
