/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_CONFIGFILE_H_INCLUDED__
#define __CUTILS_CONFIGFILE_H_INCLUDED__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct configfile;
typedef struct configfile configfile_t;

configfile_t *configfile_create(const char *data);

configfile_t *configfile_create_from_file(const char *fname);

void configfile_destroy(configfile_t *cfg);

const char *configfile_str(configfile_t *cfg, const char *name, const char *def);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_CONFIGFILE_H_INCLUDED__ */
