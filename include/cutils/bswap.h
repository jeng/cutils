/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_BSWAP_H_INCLUDED__
#define __CUTILS_BSWAP_H_INCLUDED__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

static inline uint32_t
default_swab32(uint32_t val)
{
    return (((val & 0xff000000) >> 24) |
        ((val & 0x00ff0000) >> 8) |
        ((val & 0x0000ff00) << 8) |
        ((val & 0x000000ff) << 24));
}

#undef bswap32

static inline uint16_t
default_swab16(uint16_t val)
{
    return (((val & 0xff00) >> 8) |
        ((val & 0x00ff) << 8));
}

#undef bswap16

#if defined(__GNUC__) && defined(__i386__)

#define bswap32(x) ({ \
    uint32_t __res; \
    if (__builtin_constant_p(x)) { \
        __res = default_swab32(x); \
    } else { \
        __asm__("bswap %0" : "=r" (__res) : "0" ((uint32_t)(x))); \
    } \
    __res; })

#define bswap16(x) ({ \
    uint16_t __res; \
    if (__builtin_constant_p(x)) { \
        __res = default_swab16(x); \
    } else { \
        __asm__("xchgb %b0,%h0" : "=q" (__res) : "0" ((uint16_t)(x))); \
    } \
    __res; })

#elif defined(__GNUC__) && defined(__x86_64__)

#define bswap32(x) ({ \
    uint32_t __res; \
    if (__builtin_constant_p(x)) { \
        __res = default_swab32(x); \
    } else { \
        __asm__("bswapl %0" : "=r" (__res) : "0" ((uint32_t)(x))); \
    } \
    __res; })

#define bswap16(x) ({ \
    uint16_t __res; \
    if (__builtin_constant_p(x)) { \
        __res = default_swab16(x); \
    } else { \
        __asm__("xchgb %b0,%h0" : "=Q" (__res) : "0" ((uint16_t)(x))); \
    } \
    __res; })

#elif defined(_MSC_VER) && (defined(_M_IX86) || defined(_M_X64))

#include <stdlib.h>

#define bswap32(x) _byteswap_ulong(x)
#define bswap16(x) _byteswap_ushort(x)

#endif

#ifdef bswap32

#undef ntohl
#undef htonl
#define ntohl(x) bswap32(x)
#define htonl(x) bswap32(x)

#endif

#ifdef bswap16

#undef ntohs
#undef htons
#define ntohs(x) bswap16(x)
#define htons(x) bswap16(x)

#endif

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_BSWAP_H_INCLUDED__ */
