/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_VFS_H_INCLUDED__
#define __CUTILS_VFS_H_INCLUDED__

#include <stddef.h>
#include <stdint.h>

#include "cutils/sds.h"

#ifdef __cplusplus
extern "C" {
#endif

#define VFS_SEEK_SET 0
#define VFS_SEEK_CURRENT 1
#define VFS_SEEK_END 2

struct vfs_fs;

struct vfs_file {
    struct vfs_fs *fs;
    const char *name;
    size_t size;
    size_t pos;
    int mode;
};

int vfs_setup(void);
void vfs_teardown(void);

int vfs_path_mount(const char *mount_point, struct vfs_fs *fs);
struct vfs_fs *vfs_path_get(const char *pname, sds *name_);
int vfs_path_locate(const char *pname, const char *dir, sds *pname_);

int vfs_mount(const char *mount_point, const char *type, void *cookie);
int vfs_mkdir(const char *pname, const char *dir, sds *pname_);
int vfs_remove(const char *pname);

struct vfs_file *vfs_open(const char *pname, const char *mode);
void vfs_close(struct vfs_file *f);
int vfs_read(struct vfs_file *f, void *buf_, size_t sz);
int vfs_write(struct vfs_file *f, const void *buf, size_t sz);
size_t vfs_seek(struct vfs_file *f, int pos, int where);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_VFS_H_INCLUDED__ */
