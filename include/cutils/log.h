/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_LOG_H_INCLUDED__
#define __CUTILS_LOG_H_INCLUDED__

#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef LOG_TAG
    #define LOG_TAG NULL
#endif

void logit(const char *domain, int level, const char *fmt, ...);

#define LOG_LEVEL_OFF       11
#define LOG_LEVEL_FATAL     9
#define LOG_LEVEL_ERROR     7
#define LOG_LEVEL_WARN      5
#define LOG_LEVEL_INFO      3
#define LOG_LEVEL_DEBUG     1

#define LOGF(fmt, ...)      logit(LOG_TAG, LOG_LEVEL_FATAL, fmt, ##__VA_ARGS__)
#define LOGE(fmt, ...)      logit(LOG_TAG, LOG_LEVEL_ERROR, fmt, ##__VA_ARGS__)
#define LOGW(fmt, ...)      logit(LOG_TAG, LOG_LEVEL_WARN, fmt, ##__VA_ARGS__)
#define LOGI(fmt, ...)      logit(LOG_TAG, LOG_LEVEL_INFO, fmt, ##__VA_ARGS__)
#define LOGD(fmt, ...)      logit(LOG_TAG, LOG_LEVEL_DEBUG, fmt, ##__VA_ARGS__)

#undef LOG_TAG

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_LOG_H_INCLUDED__ */
