/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_SLIST_H_INCLUDED__
#define __CUTILS_SLIST_H_INCLUDED__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct slist_node {
    struct slist_node *next;
    void *val;
};

struct slist {
    struct slist_node *head, *tail, *cur;
    size_t size;
};
typedef struct slist slist_t;

/** init slist object */
int slist_init(slist_t *sl);

/** close slist object */
void slist_close(slist_t *sl);

/** create a new slist object */
slist_t *slist_create(void);

/** destroy slist object */
void slist_destroy(slist_t *sl);

/** get first node */
void *slist_first(slist_t *sl);

/** get next node */
void *slist_next(slist_t *sl);

/** append a node to list tail */
int slist_append(slist_t *sl, void *val);

/** push a node to list head */
int slist_push(slist_t *sl, void *val);

/** pop a node from list head */
void *slist_pop(slist_t *sl);

/** remove a node */
int slist_remove(slist_t *sl, void *val);

/** make a copy of list */
slist_t *slist_copy(slist_t *sl);

/** get the list's size */
size_t slist_size(slist_t *sl);

#ifdef __cplusplus
}
#endif

#endif /* __CUTILS_SLIST_H_INCLUDED__ */
