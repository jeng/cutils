/**
 * Copyright (C) 2011 by yugi
 *
 * This file is part of cutils, distributed under the MIT License.
 * For full terms see the included LICENSE file.
 */
#ifndef __CUTILS_H_INCLUDED__
#define __CUTILS_H_INCLUDED__

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define CUTILS_VERSION "0.1.0"
#define CUTILS_VER_MAJOR 0
#define CUTILS_VER_MINOR 1
#define CUTILS_VER_REVISION 0

#include "cutils/prelude.h"
#include "cutils/zmalloc.h"
#include "cutils/error.h"
#include "cutils/array.h"
#include "cutils/slist.h"
#include "cutils/hashtable.h"

#endif /* __CUTILS_H_INCLUDED__ */
