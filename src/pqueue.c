#include <stddef.h>
#include <memory.h>

#include "cutils/prelude.h"
#include "cutils/pqueue.h"
#include "cutils/zmalloc.h"

#define left(i) ((i) << 1)
#define right(i) (((i) << 1) + 1)
#define parent(i) ((i) >> 1)


int
pqueue_init(pqueue_t *q, size_t n, pqueue_cmp_pfn cmp)
{
    if ((q->d = zmalloc((n + 1) * sizeof(void *))) == NULL)
        return -1;

    q->size = 1;
    q->avail = q->step = (n + 1); /* see comment above about n+1 */
    q->cmp = cmp;

    return 0;
}

void
pqueue_close(pqueue_t *q)
{
    zfree(q->d);
    q->d = NULL;
}

void
pqueue_clear(pqueue_t *q)
{
    q->size = 1;
}

size_t
pqueue_size(pqueue_t *q)
{
    /* queue element 0 exists but doesn't count since it isn't used */
    return (q->size - 1);
}

static void
_bubble_up(pqueue_t *q, size_t i)
{
    size_t parent_node;
    void *moving_node = q->d[i];

    for (parent_node = parent(i);
            ((i > 1) && q->cmp(q->d[parent_node], moving_node));
            i = parent_node, parent_node = parent(i)) {
        q->d[i] = q->d[parent_node];
    }

    q->d[i] = moving_node;
}


static size_t
_maxchild(pqueue_t *q, size_t i)
{
    size_t child_node = left(i);

    if (child_node >= q->size)
        return 0;

    if ((child_node + 1) < q->size &&
        q->cmp(q->d[child_node], q->d[child_node + 1]))
        child_node++; /* use right child instead of left */

    return child_node;
}


static void
_percolate_down(pqueue_t *q, size_t i)
{
    size_t child_node;
    void *moving_node = q->d[i];

    while ((child_node = _maxchild(q, i)) != 0 &&
            q->cmp(moving_node, q->d[child_node])) {
        q->d[i] = q->d[child_node];
        i = child_node;
    }

    q->d[i] = moving_node;
}

int
pqueue_insert(pqueue_t *q, void *d)
{
    void *tmp;
    size_t i;
    size_t newsize;

    if (!q)
        return -1;

    /* allocate more memory if necessary */
    if (q->size >= q->avail) {
        newsize = q->size + q->step;
        if ((tmp = zrealloc(q->d, sizeof(void *) * newsize)) == NULL)
            return -1;

        q->d = tmp;
        q->avail = newsize;
    }

    /* insert item */
    i = q->size++;
    q->d[i] = d;
    _bubble_up(q, i);

    return 0;
}

void *
pqueue_pop(pqueue_t *q)
{
    void *head;

    if (!q || q->size == 1)
        return NULL;

    head = q->d[1];
    q->d[1] = q->d[--q->size];
    _percolate_down(q, 1);

    return head;
}

void *
pqueue_peek(pqueue_t *q)
{
    if (!q || q->size == 1)
        return NULL;

    return q->d[1];
}
