#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <time.h>
#include <errno.h>

#include "cutils/prelude.h"
#include "cutils/util.h"
#include "cutils/zmalloc.h"

static int g_seed = 0x0F0F0F0F;

void
p_srand(int seed)
{
    g_seed = seed;
}

int
p_rand(void)
{
    int m = 2147483399;
    int a = 40692;
    int q = m / a;
    int r = m % a;

    g_seed = a * (g_seed % q) - r * (g_seed / q);
    if (g_seed < 0)
        g_seed += m;
    return g_seed;
}

char *
p_strcpyn(char *dst, const char *src, size_t dst_size)
{
    char *d, *end;

    if (dst_size == 0 || src == NULL)
        return dst;

    d = dst;
    end = dst + dst_size - 1;
    for (; d < end; ++d, ++src) {
        if ((*d = *src) == '\0')
            return d;
    }
    *d = '\0';
    return d;
}

char *
p_strcatn(char *dst, const char *src, size_t dst_size)
{
    size_t dstlen = strlen(dst);
    if (dst_size > 0 && dstlen < dst_size)
        p_strcpyn(dst + dstlen, src, dst_size - dstlen);
    return dst;
}

void *
p_load_file(const char *fn, unsigned *sz_)
{
    char *data;
    int sz;
    FILE *fd;

    data = 0;
    fd = fopen(fn, "r");
    if (!fd) {
        printf(" * ERROR: open %s fail: %s\n", fn,
                strerror(errno));
        return 0;
    }

    sz = fseek(fd, 0, SEEK_END);
    if (sz < 0)
        goto oops;

    if (fseek(fd, 0, SEEK_SET) != 0)
        goto oops;

    data = (char*)zmalloc(sz + 1);
    if (data == 0)
        goto oops;

    if (fread(data, 1, sz, fd) != sz)
        goto oops;
    fclose(fd);
    data[sz] = 0;

    if (sz_)
        *sz_ = sz;
    return data;

oops:
    fclose(fd);
    if (data != 0)
        zfree(data);
    return 0;
}

/* Glob-style pattern matching. */
int stringmatchlen(const char *pattern, int patternLen,
        const char *string, int stringLen, int nocase)
{
    while(patternLen) {
        switch(pattern[0]) {
        case '*':
            while (pattern[1] == '*') {
                pattern++;
                patternLen--;
            }
            if (patternLen == 1)
                return 1; /* match */
            while(stringLen) {
                if (stringmatchlen(pattern+1, patternLen-1,
                            string, stringLen, nocase))
                    return 1; /* match */
                string++;
                stringLen--;
            }
            return 0; /* no match */
            break;
        case '?':
            if (stringLen == 0)
                return 0; /* no match */
            string++;
            stringLen--;
            break;
        case '[':
        {
            int not, match;

            pattern++;
            patternLen--;
            not = pattern[0] == '^';
            if (not) {
                pattern++;
                patternLen--;
            }
            match = 0;
            while(1) {
                if (pattern[0] == '\\') {
                    pattern++;
                    patternLen--;
                    if (pattern[0] == string[0])
                        match = 1;
                } else if (pattern[0] == ']') {
                    break;
                } else if (patternLen == 0) {
                    pattern--;
                    patternLen++;
                    break;
                } else if (pattern[1] == '-' && patternLen >= 3) {
                    int start = pattern[0];
                    int end = pattern[2];
                    int c = string[0];
                    if (start > end) {
                        int t = start;
                        start = end;
                        end = t;
                    }
                    if (nocase) {
                        start = tolower(start);
                        end = tolower(end);
                        c = tolower(c);
                    }
                    pattern += 2;
                    patternLen -= 2;
                    if (c >= start && c <= end)
                        match = 1;
                } else {
                    if (!nocase) {
                        if (pattern[0] == string[0])
                            match = 1;
                    } else {
                        if (tolower((int)pattern[0]) == tolower((int)string[0]))
                            match = 1;
                    }
                }
                pattern++;
                patternLen--;
            }
            if (not)
                match = !match;
            if (!match)
                return 0; /* no match */
            string++;
            stringLen--;
            break;
        }
        case '\\':
            if (patternLen >= 2) {
                pattern++;
                patternLen--;
            }
            /* fall through */
        default:
            if (!nocase) {
                if (pattern[0] != string[0])
                    return 0; /* no match */
            } else {
                if (tolower((int)pattern[0]) != tolower((int)string[0]))
                    return 0; /* no match */
            }
            string++;
            stringLen--;
            break;
        }
        pattern++;
        patternLen--;
        if (stringLen == 0) {
            while(*pattern == '*') {
                pattern++;
                patternLen--;
            }
            break;
        }
    }
    if (patternLen == 0 && stringLen == 0)
        return 1;
    return 0;
}

int
p_strmatch(const char *pattern, const char *string, int nocase)
{
    return stringmatchlen(pattern, strlen(pattern),
            string, strlen(string), nocase);
}

long long
p_memtoll(const char *p, int *err)
{
    const char *u;
    char buf[128];
    long mul; /* unit multiplier */
    long long val;
    unsigned int digits;

    if (err)
        *err = 0;

    u = p;
    if (*u == '-') u++;
    while(*u && isdigit(*u))
        u++;
    if (*u == '\0' || !strcasecmp(u,"b")) {
        mul = 1;
    } else if (!strcasecmp(u,"k")) {
        mul = 1000;
    } else if (!strcasecmp(u,"kb")) {
        mul = 1024;
    } else if (!strcasecmp(u,"m")) {
        mul = 1000*1000;
    } else if (!strcasecmp(u,"mb")) {
        mul = 1024*1024;
    } else if (!strcasecmp(u,"g")) {
        mul = 1000L*1000*1000;
    } else if (!strcasecmp(u,"gb")) {
        mul = 1024L*1024*1024;
    } else {
        if (err)
            *err = 1;
        mul = 1;
    }
    digits = u-p;
    if (digits >= sizeof(buf)) {
        if (err)
            *err = 1;
        return LLONG_MAX;
    }
    memcpy(buf, p, digits);
    buf[digits] = '\0';
    val = strtoll(buf, NULL, 10);
    return val * mul;
}

int
p_ll2str(char *s, size_t len, long long value)
{
    char buf[32], *p;
    unsigned long long v;
    size_t l;

    if (len == 0)
        return 0;
    v = (value < 0) ? -value : value;
    p = buf + 31; /* point to the last character */
    do {
        *p-- = '0' + (v % 10);
        v /= 10;
    } while (v);
    if (value < 0)
        *p-- = '-';
    p++;
    l = 32 - (p - buf);
    if (l + 1 > len)
        l = len - 1;
    memcpy(s, p, l);
    s[l] = '\0';
    return l;
}

int
p_str2ll(char *s, size_t slen, long long *value)
{
    char *p = s;
    size_t plen = 0;
    int negative = 0;
    unsigned long long v;

    if (plen == slen)
        return 0;

    /* Special case: first and only digit is 0. */
    if (slen == 1 && p[0] == '0') {
        if (value != NULL) *value = 0;
        return 1;
    }

    if (p[0] == '-') {
        negative = 1;
        p++; plen++;

        /* Abort on only a negative sign. */
        if (plen == slen)
            return 0;
    }

    /* First digit should be 1-9, otherwise the string should just be 0. */
    if (p[0] >= '1' && p[0] <= '9') {
        v = p[0]-'0';
        p++; plen++;
    } else if (p[0] == '0' && slen == 1) {
        *value = 0;
        return 1;
    } else {
        return 0;
    }

    while (plen < slen && p[0] >= '0' && p[0] <= '9') {
        if (v > (ULLONG_MAX / 10)) /* Overflow. */
            return 0;
        v *= 10;

        if (v > (ULLONG_MAX - (p[0]-'0'))) /* Overflow. */
            return 0;
        v += p[0]-'0';

        p++; plen++;
    }

    /* Return if not all bytes were used. */
    if (plen < slen)
        return 0;

    if (negative) {
        if (v > ((unsigned long long)(-(LLONG_MIN+1))+1)) /* Overflow. */
            return 0;
        if (value != NULL) *value = -v;
    } else {
        if (v > LLONG_MAX) /* Overflow. */
            return 0;
        if (value != NULL) *value = v;
    }
    return 1;
}

int
p_str2l(char *s, size_t slen, long *lval)
{
    long long llval;

    if (!p_str2ll(s, slen, &llval))
        return 0;

    if (llval < LONG_MIN || llval > LONG_MAX)
        return 0;

    *lval = (long)llval;
    return 1;
}

/* Convert a double to a string representation. Returns the number of bytes
 * required. The representation should always be parsable by stdtod(3). */
int d2string(char *buf, size_t len, double value) {
    if (isnan(value)) {
        len = snprintf(buf,len,"nan");
    } else if (isinf(value)) {
        if (value < 0)
            len = snprintf(buf,len,"-inf");
        else
            len = snprintf(buf,len,"inf");
    } else if (value == 0) {
        /* See: http://en.wikipedia.org/wiki/Signed_zero, "Comparisons". */
        if (1.0/value < 0)
            len = snprintf(buf,len,"-0");
        else
            len = snprintf(buf,len,"0");
    } else {
#if (DBL_MANT_DIG >= 52) && (LLONG_MAX == 0x7fffffffffffffffLL)
        /* Check if the float is in a safe range to be casted into a
         * long long. We are assuming that long long is 64 bit here.
         * Also we are assuming that there are no implementations around where
         * double has precision < 52 bit.
         *
         * Under this assumptions we test if a double is inside an interval
         * where casting to long long is safe. Then using two castings we
         * make sure the decimal part is zero. If all this is true we use
         * integer printing function that is much faster. */
        double min = -4503599627370495; /* (2^52)-1 */
        double max = 4503599627370496; /* -(2^52) */
        if (val > min && val < max && value == ((double)((long long)value)))
            len = ll2string(buf,len,(long long)value);
        else
#endif
            len = snprintf(buf,len,"%.17g",value);
    }

    return len;
}

int
p_prefixcmp(const char *str, const char *prefix)
{
    for (;;) {
        char p = *(prefix++), s;
        if (!p)
            return 0;
        if ((s = *(str++)) != p)
            return s - p;
    }
}

int
p_suffixcmp(const char *str, const char *suffix)
{
    size_t a = strlen(str);
    size_t b = strlen(suffix);
    if (a < b)
        return -1;
    return strcmp(str + (a - b), suffix);
}

char *
p_strtok(char **end, const char *sep)
{
    char *ptr = *end;

    while (*ptr && strchr(sep, *ptr))
        ++ptr;

    if (*ptr) {
        char *start = ptr;
        *end = start + 1;

        while (**end && !strchr(sep, **end))
            ++*end;

        if (**end) {
            **end = '\0';
            ++*end;
        }

        return start;
    }

    return NULL;
}

void
p_hexdump(const char *buffer, size_t len)
{
    static const size_t LINE_WIDTH = 16;

    size_t line_count, last_line, i, j;
    const char *line;

    line_count = (len / LINE_WIDTH);
    last_line = (len % LINE_WIDTH);

    for (i = 0; i < line_count; ++i) {
        line = buffer + (i * LINE_WIDTH);
        for (j = 0; j < LINE_WIDTH; ++j, ++line)
            printf("%02X ", (unsigned char)*line & 0xFF);

        printf("| ");

        line = buffer + (i * LINE_WIDTH);
        for (j = 0; j < LINE_WIDTH; ++j, ++line)
            printf("%c", (*line >= 32 && *line <= 126) ? *line : '.');

        printf("\n");
    }

    if (last_line > 0) {

        line = buffer + (line_count * LINE_WIDTH);
        for (j = 0; j < last_line; ++j, ++line)
            printf("%02X ", (unsigned char)*line & 0xFF);

        for (j = 0; j < (LINE_WIDTH - last_line); ++j)
            printf("    ");

        printf("| ");

        line = buffer + (line_count * LINE_WIDTH);
        for (j = 0; j < last_line; ++j, ++line)
            printf("%c", (*line >= 32 && *line <= 126) ? *line : '.');

        printf("\n");
    }

    printf("\n");
}

long long
p_ustime(void)
{
    struct timeval tv;
    long long ust;

    gettimeofday(&tv, NULL);
    ust = ((long long)tv.tv_sec) * 1000000;
    ust += tv.tv_usec;
    return ust;
}

int
p_is_little_endian(void)
{
    union {
        uint16_t
            u;
        uint8_t
            b;
    } tmp;
    tmp.u = 1;
    return (tmp.b == 1) ? 1 : 0;
}

void
p_mssleep(int ms)
{
    int was_error;
    struct timespec elapsed, tv;

    elapsed.tv_sec = ms / 1000;
    elapsed.tv_nsec = (ms % 1000) * 1000000;

    do {
        errno = 0;

        tv.tv_sec = elapsed.tv_sec;
        tv.tv_nsec = elapsed.tv_nsec;
        was_error = nanosleep(&tv, &elapsed);
    } while (was_error && (errno == EINTR));
}
