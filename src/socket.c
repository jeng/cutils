#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>

#include "cutils/prelude.h"
#include "cutils/socket.h"
#include "cutils/zmalloc.h"

#if defined(__sun)
    #define AF_LOCAL AF_UNIX
#endif

static void
_socket_set_error(char *err, const char *fmt, ...)
{
    va_list ap;
    if (!err)
        return;
    va_start(ap, fmt);
    vsnprintf(err, 256, fmt, ap);
    va_end(ap);
}

int
socket_set_nonblock(char *err, int fd)
{
    int flags;

    if ((flags = fcntl(fd, F_GETFL)) == -1) {
        _socket_set_error(err, "fcntl(F_GETFL): %s", strerror(errno));
        return -1;
    }
    if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
        _socket_set_error(err, "fcntl(F_SETFL,O_NONBLOCK): %s", strerror(errno));
        return -1;
    }
    return 0;
}

int
socket_set_tcpnodelay(char *err, int fd)
{
    int yes = 1;
    if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(yes)) == -1) {
        _socket_set_error(err, "setsockopt TCP_NODELAY: %s", strerror(errno));
        return -1;
    }
    return 0;
}

int
socket_set_sendbuffer(char *err, int fd, int buffsize)
{
    if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, &buffsize, sizeof(buffsize)) == -1) {
        _socket_set_error(err, "setsockopt SO_SNDBUF: %s", strerror(errno));
        return -1;
    }
    return 0;
}

int
socket_set_tcpkeepalive(char *err, int fd)
{
    int yes = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &yes, sizeof(yes)) == -1) {
        _socket_set_error(err, "setsockopt SO_KEEPALIVE: %s", strerror(errno));
        return -1;
    }
    return 0;
}

int
socket_resolve(char *err, char *host, char *ipbuf)
{
    struct sockaddr_in sa;
    struct hostent *he;

    sa.sin_family = AF_INET;
    if (inet_aton(host, &sa.sin_addr) == 0) {
        he = gethostbyname(host);
        if (!he) {
            _socket_set_error(err, "can't resolve: %s", host);
            return -1;
        }
        memcpy(&sa.sin_addr, he->h_addr, sizeof(struct in_addr));
    }
    strcpy(ipbuf, inet_ntoa(sa.sin_addr));
    return 0;
}

static int
_socket_create(char *err, int domain)
{
    int s, on = 1;

    if ((s = socket(domain, SOCK_STREAM, 0)) == -1) {
        _socket_set_error(err, "creating socket: %s", strerror(errno));
        return -1;
    }
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1) {
        _socket_set_error(err, "setsockopt SO_REUSEADDR: %s", strerror(errno));
        return -1;
    }
    return s;
}

static int
_socket_tcp_connect(char *err, char *addr, int port, int nonblock)
{
    int s;
    struct sockaddr_in sa;
    struct hostent *he;

    if ((s = _socket_create(err,AF_INET)) == -1)
        return -1;

    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    if (inet_aton(addr, &sa.sin_addr) == 0) {
        he = gethostbyname(addr);
        if (he == NULL) {
            _socket_set_error(err, "can't resolve: %s", addr);
            close(s);
            return -1;
        }
        memcpy(&sa.sin_addr, he->h_addr, sizeof(struct in_addr));
    }
    if (nonblock) {
        if (socket_set_nonblock(err, s) != 0)
            return -1;
    }
    if (connect(s, (struct sockaddr*)&sa, sizeof(sa)) == -1) {
        if (errno == EINPROGRESS && nonblock)
            return s;
        _socket_set_error(err, "connect: %s", strerror(errno));
        close(s);
        return -1;
    }
    return s;
}

int
socket_tcp_connect(char *err, char *addr, int port, int nonblock)
{
    return _socket_tcp_connect(err, addr, port, nonblock);
}

int
_socket_unix_connect(char *err, char *path, int nonblock)
{
    int s;
    struct sockaddr_un sa;

    if ((s = _socket_create(err, AF_LOCAL)) == -1)
        return -1;

    sa.sun_family = AF_LOCAL;
    strncpy(sa.sun_path,path,sizeof(sa.sun_path)-1);
    if (nonblock) {
        if (socket_set_nonblock(err,s) != 0)
            return -1;
    }
    if (connect(s,(struct sockaddr*)&sa,sizeof(sa)) == -1) {
        if (errno == EINPROGRESS && nonblock)
            return s;
        _socket_set_error(err, "connect: %s", strerror(errno));
        close(s);
        return -1;
    }
    return s;
}

int
socket_unix_connect(char *err, char *path, int nonblock)
{
    return _socket_unix_connect(err, path, nonblock);
}

int
socket_read(int fd, char *buf, int count)
{
    int nread, totlen = 0;
    while (totlen != count) {
        nread = read(fd, buf, count-totlen);
        if (nread == 0)
            return totlen;
        if (nread == -1)
            return -1;
        totlen += nread;
        buf += nread;
    }
    return totlen;
}

int
socket_write(int fd, char *buf, int count)
{
    int nwritten, totlen = 0;
    while (totlen != count) {
        nwritten = write(fd,buf,count-totlen);
        if (nwritten == 0)
            return totlen;
        if (nwritten == -1)
            return -1;
        totlen += nwritten;
        buf += nwritten;
    }
    return totlen;
}

static int
_socket_listen(char *err, int s, struct sockaddr *sa, socklen_t len)
{
    if (bind(s, sa, len) == -1) {
        _socket_set_error(err, "bind: %s", strerror(errno));
        close(s);
        return -1;
    }
    if (listen(s, 511) == -1) {
        _socket_set_error(err, "listen: %s", strerror(errno));
        close(s);
        return -1;
    }
    return 0;
}

int
socket_tcp_server(char *err, int port, char *bindaddr)
{
    int s;
    struct sockaddr_in sa;

    if ((s = _socket_create(err, AF_INET)) == -1)
        return -1;

    memset(&sa,0,sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bindaddr && inet_aton(bindaddr, &sa.sin_addr) == 0) {
        _socket_set_error(err, "invalid bind address");
        close(s);
        return -1;
    }
    if (_socket_listen(err,s,(struct sockaddr*)&sa,sizeof(sa)) == -1)
        return -1;
    return s;
}

int
socket_unix_server(char *err, char *path, mode_t perm)
{
    int s;
    struct sockaddr_un sa;

    if ((s = _socket_create(err,AF_LOCAL)) == -1)
        return -1;

    memset(&sa,0,sizeof(sa));
    sa.sun_family = AF_LOCAL;
    strncpy(sa.sun_path,path,sizeof(sa.sun_path)-1);
    if (_socket_listen(err,s,(struct sockaddr*)&sa,sizeof(sa)) == -1)
        return -1;
    if (perm)
        chmod(sa.sun_path, perm);
    return s;
}

static int
_socket_accept(char *err, int s, struct sockaddr *sa, socklen_t *len)
{
    int fd;
    while (1) {
        fd = accept(s, sa, len);
        if (fd == -1) {
            if (errno == EINTR) {
                continue;
            } else {
                _socket_set_error(err, "accept: %s", strerror(errno));
                return -1;
            }
        }
        break;
    }
    return fd;
}

int
socket_tcp_accept(char *err, int s, char *ip, int *port)
{
    int fd;
    struct sockaddr_in sa;
    socklen_t salen = sizeof(sa);
    if ((fd = _socket_accept(err,s,(struct sockaddr*)&sa,&salen)) == -1)
        return -1;
    if (ip) strcpy(ip,inet_ntoa(sa.sin_addr));
    if (port) *port = ntohs(sa.sin_port);
    return fd;
}

int
socket_unix_accept(char *err, int s)
{
    int fd;
    struct sockaddr_un sa;
    socklen_t salen = sizeof(sa);
    if ((fd = _socket_accept(err,s,(struct sockaddr*)&sa,&salen)) == -1)
        return -1;

    return fd;
}

int
socket_peertostring(int fd, char *ip, int *port)
{
    struct sockaddr_in sa;
    socklen_t salen = sizeof(sa);

    if (getpeername(fd, (struct sockaddr*)&sa, &salen) == -1)
        return -1;
    if (ip)
        strcpy(ip, inet_ntoa(sa.sin_addr));
    if (port)
        *port = ntohs(sa.sin_port);
    return 0;
}
