#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "cutils/prelude.h"
#include "cutils/configfile.h"
#include "cutils/zmalloc.h"
#include "cutils/sds.h"
#include "cutils/hashtable.h"
#include "cutils/util.h"

struct configfile {
    hashtable_t *ht;
};

configfile_t *
configfile_create(const char *data)
{
    char *beg, *end;
    int len;
    sds_t s = sdsempty();
    sds_t stk = sdsempty();
    sds_t sk = sdsempty();
    int stk_depth = 0;

    configfile_t *cfg = (configfile_t *)zmalloc(sizeof(*cfg));
    cfg->ht = hashtable_create(hashtable_str_hash,
            hashtable_str_cmp, zfree, zfree);

    beg = (char *)data;
    do {
        end = strchr(beg, '\n');
        if (!end)
            break;
        len = end - beg;
        s = sdscpylen(s, beg, len);
        s = sdstrim(s, " \t\n");
        if (s[0] == '#' || s[0] == ';') {
            /* comment */
        } else {
            int cnt;
            sds *tokens = sdssplitargs(s, &cnt);
            if (cnt == 0) {
                /* empty line */
            } else if (cnt == 1 && !(strcmp(tokens[0], "}"))) {
                if (stk_depth <= 0)
                    break;
                if (stk_depth == 1) {
                    stk = sdscpy(stk, "");
                } else {
                    stk[strlen(stk) - 1] = '\0';
                    char *p = strrchr(stk, '.');
                    if (!p)
                        break;
                    *(p + 1) = '\0';
                    sdsupdatelen(stk);
                }
                stk_depth--;
            } else if (cnt == 2 && !(strcmp(tokens[1], "{"))) {
                stk = sdscat(stk, tokens[0]);
                stk = sdscat(stk, ".");
                stk_depth++;
            } else if (cnt == 2 || cnt == 3) {
                char *k, *v;
                k = tokens[0];
                v = tokens[(cnt == 2) ? 1 : 2];
                sk = sdscpy(sk, stk);
                sk = sdscat(sk, k);
                hashtable_set(cfg->ht, zstrdup(sk), zstrdup(v));
            }
            sdsfreesplitres(tokens, cnt);
        }
        beg = end + 1;
    } while (1);

    sdsfree(s);
    sdsfree(sk);
    sdsfree(stk);
    return cfg;
}

configfile_t *
configfile_create_from_file(const char *fname)
{
    void *data = p_load_file(fname, NULL);
    if (!data)
        return NULL;
    configfile_t *cfg = configfile_create(data);
    zfree(data);
    return cfg;
}

void
configfile_destroy(configfile_t *cfg)
{
    hashtable_destroy(cfg->ht);
    zfree(cfg);
}

const char *
configfile_str(configfile_t *cfg, const char *name, const char *def)
{
    const char *val = (const char *)hashtable_get(cfg->ht, name);
    if (!val)
        return def;
    else
        return val;
}
