#include <stddef.h>
#include <string.h>

#include "cutils/prelude.h"
#include "cutils/array.h"
#include "cutils/zmalloc.h"

int
array_init(array_t *arr, int nelem, size_t selem)
{
    if (nelem < 1)
        nelem = 1;

    arr->selem = selem;
    arr->nelem = 0;
    arr->nalloc = nelem;

    arr->elems = zmalloc(nelem * selem);
    if (!arr->elems) {
        zfree(arr);
        return -1;
    }
    return 0;
}

void
array_close(array_t *arr)
{
    zfree(arr->elems);
    arr->elems = NULL;
    arr->nelem = 0;
}

void *
array_push(array_t *arr)
{
    char *newelems;
    int newnum;

    if (arr->nelem == arr->nalloc) {
        newnum = (arr->nalloc <= 0) ? 1 : arr->nalloc << 1;
        newelems = zrealloc(arr->elems, newnum * arr->selem);
        if (!newelems)
            return NULL;
        arr->elems = newelems;
        arr->nalloc = newnum;
    }
    return arr->elems + (arr->nelem++) * arr->selem;
}

void *
array_pop(array_t *arr)
{
    if (array_isempty(arr))
        return NULL;
    return arr->elems + (--arr->nelem) * arr->selem;
}

void
array_clear(array_t *arr)
{
    arr->nelem = 0;
}
