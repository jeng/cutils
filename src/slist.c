#include <stddef.h>
#include <memory.h>

#include "cutils/prelude.h"
#include "cutils/slist.h"
#include "cutils/zmalloc.h"

#define p_clear(x) memset((x), 0, sizeof(*(x)))

int
slist_init(slist_t *sl)
{
    p_clear(sl);
    return 0;
}

void
slist_close(slist_t *sl)
{
    struct slist_node *cur, *next;
    for (cur = sl->head; cur; cur = next) {
        next = cur->next;
        zfree(cur);
    }
}

slist_t *
slist_create(void)
{
    slist_t *sl = (slist_t *)zmalloc(sizeof(*sl));
    slist_init(sl);
    return sl;
}

void
slist_destroy(slist_t *sl)
{
    slist_close(sl);
    zfree(sl);
}

void *
slist_first(slist_t *sl)
{
    sl->cur = sl->head;
    if (sl->head)
        return sl->head->val;
    return NULL;
}

void *
slist_next(slist_t *sl)
{
    if (sl->cur)
        sl->cur = sl->cur->next;
    else
        sl->cur = sl->head;
    if (sl->cur)
        return sl->cur->val;
    return NULL;
}

int
slist_append(slist_t *sl, void *val)
{
    struct slist_node *n;

    n = (struct slist_node *)zmalloc(sizeof(*n));
    if (!n)
        return -1;
    p_clear(n);

    n->val = val;
    if (sl->tail)
        sl->tail->next = n;
    else
        sl->head = n;
    sl->tail = n;

    n->next = NULL;
    sl->size++;
    sl->cur = NULL;
    return 0;
}

int
slist_push(slist_t *sl, void *val)
{
    struct slist_node *n;

    n = (struct slist_node *)zmalloc(sizeof(*n));
    if (!n)
        return -1;
    p_clear(n);

    n->val = val;
    n->next = sl->head;
    sl->head = n;
    if (!sl->tail)
        sl->tail = n;

    sl->size++;
    sl->cur = NULL;
    return 0;
}

void *
slist_pop(slist_t *sl)
{
    struct slist_node *n = sl->head;
    void *val = NULL;

    if (n) {
        val = n->val;
        sl->head = n->next;
        if (sl->tail == n)
            sl->tail = NULL;
        zfree(n);
    }

    sl->size--;
    sl->cur = NULL;
    return val;
}

int
slist_remove(slist_t *sl, void *val)
{
    struct slist_node *cur, *pre = NULL;

    /* locate the node */
    for (cur = sl->head; cur; cur = cur->next) {
        if (cur->val == val)
            break;
        pre = cur;
    }
    if (cur) {
        if (pre)
            pre->next = cur->next;
        else
            sl->head = cur->next;
        if (!cur->next)
            sl->tail = pre;
        zfree(cur);
        sl->size--;
        sl->cur = NULL;
        return 0;
    }
    return -1;
}

slist_t *
slist_copy(slist_t *sl)
{
    slist_t *copy;
    struct slist_node *cur;

    if (!sl)
        return NULL;
    copy = slist_create();
    if (!copy)
        return NULL;

    for (cur = sl->head; cur; cur = cur->next)
        slist_append(copy, cur->val);
    return copy;
}

size_t
slist_size(slist_t *sl)
{
    return sl->size;
}
