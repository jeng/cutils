#include <stddef.h>
#include <stdarg.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "cutils/prelude.h"
#include "cutils/error.h"
#include "cutils/zmalloc.h"

/* TODO: thread safe
 */
static char g_last_error[1024];

static struct {
    int num;
    const char *str;
} _error_codes[] = {
    {CUTILS_ERR, "Unspecified error"},
    {CUTILS_ENOMEM, "Not enough space available."},
    {CUTILS_ENOTFOUND, "Object does not exist in the scope searched."},
    {CUTILS_EOSERR, "Consult the OS error information."}
};

const char *
error_last(void)
{
    if (!g_last_error[0])
        return NULL;
    return g_last_error;
}

const char *
error_tostr(int err)
{
    size_t i;

    if (err == CUTILS_EOSERR)
        return (const char *)strerror(err);
    for (i = 0; i < CUTILS_ARRAY_SIZE(_error_codes); i++)
        if (err == _error_codes[i].num)
            return _error_codes[i].str;

    return "Unknown error.";
}

void
error_clear(void)
{
    g_last_error[0] = '\0';
}

int
error_throw(int err, const char *msg, ...)
{
    va_list va;

    va_start(va, msg);
    vsnprintf(g_last_error, sizeof(g_last_error), msg, va);
    va_end(va);

    return err;
}

int
error_rethrow(int err, const char *msg, ...)
{
    char new_error[1024];
    char *old_error = NULL;
    va_list va;

    va_start(va, msg);
    vsnprintf(new_error, sizeof(new_error), msg, va);
    va_end(va);

    old_error = zstrdup(g_last_error);
    snprintf(g_last_error, sizeof(g_last_error), "%s \n  - %s", new_error, old_error);
    zfree(old_error);

    return err;
}
