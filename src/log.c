#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#include "cutils/prelude.h"
#include "cutils/log.h"

static const char *
_level2str(int level)
{
    switch (level) {
        case LOG_LEVEL_DEBUG: return "DEBUG"; break;
        case LOG_LEVEL_INFO: return "INFO"; break;
        case LOG_LEVEL_WARN: return "WARN"; break;
        case LOG_LEVEL_ERROR: return "ERROR"; break;
        case LOG_LEVEL_FATAL: return "FATAL"; break;
        default: return "NULL"; break;
    }
}

static const char *
_level2color(int level)
{
    switch (level) {
        case LOG_LEVEL_DEBUG: return "\e[1;34m"; break;
        case LOG_LEVEL_INFO: return "\e[1;32m"; break;
        case LOG_LEVEL_WARN: return "\e[1;33m"; break;
        case LOG_LEVEL_ERROR: return "\e[1;31m"; break;
        case LOG_LEVEL_FATAL: return "\e[0;31m"; break;
        default: return ""; break;
    }
}

void
logit(const char *domain, int level, const char *fmt, ...)
{
    va_list args;
    time_t tm = time(NULL);
    char time_str[32];
    strftime(time_str, 32, "%Y-%m-%d %H:%M:%S", localtime(&tm));
    fprintf(stderr, "%s(%s) {%s} [%s] ", _level2color(level),
            time_str,
            (domain == NULL) ? "null" : domain,
            _level2str(level));
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
    fprintf(stderr, "\e[0m\n");
}
