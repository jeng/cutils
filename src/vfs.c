#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cutils/prelude.h"
#include "cutils/vfs.h"
#include "cutils/zmalloc.h"
#include "cutils/sds.h"
#include "cutils/hashtable.h"

#define PATH_MAX 4096
#define NONE_EXIST (size_t)(-1)
#define MODE_READ 1
#define MODE_WRITE 2

struct vfs_fs {
    void *fs;
    void (*destroy)(void *fs);
    /* 'd' for mkdir , 'r' for remove */
    int (*op)(void *fs, const char *name, char op);
    struct vfs_file *(*open)(void *fs, const char *name, int mode);
    void (*close)(void *fs, struct vfs_file *f);
    int (*read)(void *fs, struct vfs_file *f, void *buf_, size_t sz);
    int (*write)(void *fs, struct vfs_file *f, const void *buf, size_t sz);
    int (*seek)(void *fs, struct vfs_file *f);
    size_t (*size)(void *fs, struct vfs_file *f);
};

/* forward decl
 */

void *nativefs_create(void *cookie);
void nativefs_destroy(void *fs);
int nativefs_op(void *fs, const char *name, char op);
struct vfs_file *nativefs_open(void *fs, const char *name, int mode);
void nativefs_close(void *fs, struct vfs_file *f);
int nativefs_read(void *fs, struct vfs_file *f, void *buf_, size_t sz);
int nativefs_write(void *fs, struct vfs_file *f, const void *buf, size_t sz);
int nativefs_seek(void *fs, struct vfs_file *f);
size_t nativefs_size(void *fs, struct vfs_file *f);

/* global data
 */

static hashtable_t g_vfs_mount;

static void
_free_vfs(void *fs)
{
    struct vfs_fs *vfs = (struct vfs_fs *)fs;
    if (vfs->destroy)
        vfs->destroy(vfs->fs);
    zfree(vfs);
}

/* vfs_path
 */

int
vfs_path_mount(const char *mount_point, struct vfs_fs *fs)
{
    if (hashtable_get(&g_vfs_mount, mount_point))
        return -1;
    return hashtable_set(&g_vfs_mount, zstrdup(mount_point), fs);
}

struct vfs_fs *
vfs_path_get(const char *pname, sds *name_)
{
    char *p = strchr(pname, ':');
    sds prefix = sds_nil;
    struct vfs_fs *fs = NULL;

    if (!p) {
        *name_ = sds_nil;
        return NULL;
    }

    prefix = sdsnewlen(pname, p - pname);
    *name_ = sdsnew(p + 1);
    fs = hashtable_get(&g_vfs_mount, prefix);
    if (!fs) {
        sdsfree(prefix);
        sdsfree(*name_);
        *name_ = sds_nil;
        return NULL;
    } {
        sdsfree(prefix);
        return fs;
    }
}

int
vfs_path_locate(const char *pname, const char *dir, sds *pname_)
{
    return -1;
}

/* vfs
 */

int
vfs_mount(const char *mount_point, const char *type, void *cookie)
{
    struct vfs_fs *fs = NULL;

    if (!strcmp(type, "native")) {
        void *f = nativefs_create(cookie);
        if (!f)
            return -1;
        fs = zmalloc(sizeof(*fs));
        memset(fs, 0, sizeof(*fs));
        fs->fs = f;
        fs->destroy = nativefs_destroy;
        fs->op = nativefs_op;
        fs->open = nativefs_open;
        fs->close = nativefs_close;
        fs->read = nativefs_read;
        fs->write = nativefs_write;
        fs->seek = nativefs_seek;
        fs->size = nativefs_size;
    } else {
        return -1;
    }
    return vfs_path_mount(mount_point, fs);
}

int
vfs_mkdir(const char *pname, const char *dir, sds *pname_)
{
    int rc = -1;
    sds name = sds_nil;
    struct vfs_fs *fs = vfs_path_get(pname, &name);

    *pname_ = sds_nil;
    if (fs && !fs->op(fs->fs, name, 'd'))
        rc = vfs_path_locate(pname, dir, pname_);
    sdsfree(name);
    return rc;
}

int
vfs_remove(const char *pname)
{
    int rc = -1;
    sds name = sds_nil;
    struct vfs_fs *fs = vfs_path_get(pname, &name);

    if (fs)
        rc = fs->op(fs->fs, name, 'r');
    sdsfree(name);
    return rc;
}

struct vfs_file *
vfs_open(const char *pname, const char *m)
{
    struct vfs_file *f = NULL;
    sds name = sds_nil;
    struct vfs_fs *fs = vfs_path_get(pname, &name);
    int mode = 0;

    if (!fs)
        goto done;

    if (strchr(m, 'r')) {
        mode |= MODE_READ;
        if (fs->read == NULL)
            goto done;
    }
    if (strchr(m, 'w')) {
        mode |= MODE_WRITE;
        if (fs->write == NULL)
            goto done;
    }

    f = fs->open(fs->fs, name, mode);
    if (!f)
        goto done;

    size_t sz = fs->size(fs->fs, f);
    if (sz == NONE_EXIST) {
        vfs_close(f);
        goto done;
    }

    f->fs = fs;
    f->name = name;
    f->size = sz;
    f->pos = 0;
    f->mode = mode;

done:
    if (!f)
        sdsfree(name);
    return f;
}

void 
vfs_close(struct vfs_file *f)
{
    if (f->fs->close)
        f->fs->close(f->fs->fs, f);
    sdsfree((sds)f->name);
    zfree(f);
}

int
vfs_read(struct vfs_file *f, void *buf_, size_t sz)
{
    int n = f->fs->read(f->fs->fs, f, buf_, sz);
    if (n >= 0)
        f->pos += n;
    return n;
}

int
vfs_write(struct vfs_file *f, const void *buf, size_t sz)
{
    int n = f->fs->write(f->fs->fs, f, buf, sz);
    if (n >= 0)
        f->pos += n;
    return n;
}

size_t
vfs_seek(struct vfs_file *f, int pos, int where)
{
    int current = (int)f->pos;

    switch (where) {
        case VFS_SEEK_SET:
            current = pos;
            break;
        case VFS_SEEK_CURRENT:
            current += pos;
            break;
        case VFS_SEEK_END:
            current = (int)f->size + pos;
            break;
    }

    if (current < 0) {
        current = 0;
    } else if (current > (int)f->size) {
        current = (int)f->size;
    }

    f->pos = (size_t)current;
    if (f->fs->seek)
        f->fs->seek(f->fs->fs, f);
    return f->pos;
}

int
vfs_setup(void)
{
    int rc = -1;
    hashtable_init(&g_vfs_mount, hashtable_str_hash,
        hashtable_str_cmp,
        zfree, _free_vfs);
    rc = vfs_mount(".", "native", ".");
    return rc;
}

void
vfs_teardown(void)
{
    hashtable_close(&g_vfs_mount);
}

/* nativefs
 */

struct nativefs {
    sds root;
};

struct nativefs_file {
    struct vfs_file file;
    FILE *fd;
};

void *
nativefs_create(void *cookie)
{
    struct nativefs *fs;
    if (!cookie)
        cookie = ".";

    fs = (struct nativefs *)zmalloc(sizeof(*fs));
    if (!fs)
        return NULL;
    fs->root = sdsnew(cookie);
    return fs;
}

void
nativefs_destroy(void *fs)
{
    struct nativefs *nfs = (struct nativefs *)fs;
    sdsfree(nfs->root);
    zfree(nfs);
}

int
nativefs_op(void *fs, const char *name, char op)
{
    return -1;
}

struct vfs_file *
nativefs_open(void *fs, const char *name, int mode)
{
    return NULL;
}

void
nativefs_close(void *fs, struct vfs_file *f)
{
    struct nativefs_file *nf = (struct nativefs_file *)f;
    fclose(nf->fd);
}

int
nativefs_read(void *fs, struct vfs_file *f, void *buf_, size_t sz)
{
    struct nativefs_file *nf = (struct nativefs_file *)f;
    size_t nread = fread(buf_, 1, (size_t)sz, nf->fd);
    if (nread != sz)
        return -1;
    return (int)nread;
}

int
nativefs_write(void *fs, struct vfs_file *f, const void *buf, size_t sz)
{
    struct nativefs_file *nf = (struct nativefs_file *)f;
    size_t nwrite = fwrite(buf, 1, (size_t)sz, nf->fd);
    if (nwrite != sz)
        return -1;
    return (int)nwrite;
}

int
nativefs_seek(void *fs, struct vfs_file *f)
{
    return -1;
}

size_t
nativefs_size(void *fs, struct vfs_file *f)
{
    return 0;
}
